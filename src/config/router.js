import React from "react";

const HOME = React.lazy(() => import("../pages/Home"));
const LOGIN = React.lazy(() => import("../pages/Login"));
const PRODUCT_DETAIL = React.lazy(() => import("../pages/ProductDetail"));
const PAGE_404 = React.lazy(() => import("../components/404"));

const routes = [
  {
    path: "/profile",
    exact: true,
    component: <>profile</>,
    private: true,
  },
  {
    path: "/detail",
    exact: true,
    component: PRODUCT_DETAIL,
  },

  {
    path: "/login",
    exact: true,
    component: LOGIN,
  },
  {
    path: "/",
    exact: true,
    component: HOME,
  },
  {
    path: "*",
    component: PAGE_404,
  },
];

export default routes;
