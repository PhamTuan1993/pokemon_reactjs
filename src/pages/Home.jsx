import React, { useEffect, useState } from "react";
import { Col, Container, FormControl, InputGroup, Row } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import PaginationCus from "../components/Pagination";
import PokemonItem from "../components/PokemonItem";
import {
  getListPokemonAction,
  selectLoading,
  selectPokemon
} from "../slice/pokemonSlice";
import searchFunc from "../utils/searchFunc";

export default function Home() {
  const dispatch = useDispatch();

  const pokemon = useSelector(selectPokemon);
  const isLoading = useSelector(selectLoading);

  const [filter, setFilter] = useState({
    search: "",
    sort: 0,
    page: 1,
  });
  const [pokemonRender, setPokemonRender] = useState([]);

  useEffect(() => {
    dispatch(getListPokemonAction({ limit: 100 }));
  }, []);

  useEffect(() => {
    setPokemonRender(pokemon.slice(0, 12));
  }, [pokemon]);

  const handleSearchChange = (e) => {
    const { value } = e.target;
    setFilter({ ...filter, search: value });
    const data = searchFunc(pokemon, value);
    setPokemonRender(data);
  };

  const handlePagination = (data) => {
    setFilter({ ...filter, page: data });
    setPokemonRender(pokemon.slice(calcPagi(data)[0], calcPagi(data)[1]));
  };

  const calcPagi = (data) => {
    return [(data - 1) * 12, data * 12];
  };

  return (
    <div>
      <Container className="my-4">
        <Row>
          <Col xs={4}>
            <InputGroup>
              <FormControl
                placeholder="Tim kiem ..."
                onKeyUp={handleSearchChange}
              />
            </InputGroup>
          </Col>
        </Row>
      </Container>
      <Container>
        {!isLoading ? (
          <Row>
            {pokemonRender?.map((e, i) => (
              <PokemonItem {...e} key={i} />
            ))}
          </Row>
        ) : (
          "loading..."
        )}
      </Container>
      <Container>
        <Row>
          <PaginationCus
            total={pokemon?.length || 0}
            active={filter.page}
            onPageChange={handlePagination}
          />
        </Row>
      </Container>
    </div>
  );
}
