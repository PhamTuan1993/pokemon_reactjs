import { useEffect, useState } from "react";

const fakeLogin = new Promise((resolve) => {
  return resolve({
    fullName: "Duyss",
  });
});

export default function useAuth() {
  const [user, setUser] = useState(null);

  useEffect(() => {
    const getUser = async () => {
      const user = await fakeLogin;
      setUser(user);
    };
    getUser();
  }, []);

  return { user };
}
