export default function parsePokemonData(data) {
  if (!data) return;
  if (Array.isArray(data)) {
    return data.map((e) => ({
      name: e.name,
      img: e.sprites.other["official-artwork"].front_default,
      types: e.types,
    }));
  }
  return [
    {
      name: data.name,
      img: data.sprites.other["official-artwork"].front_default,
      types: data.types,
    },
  ];
}
