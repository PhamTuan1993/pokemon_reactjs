import React from "react";

export default function searchFunc(data, query) {
  if (!query) return data.slice(0, 12);
  return data.filter((element) => {
    return element.name.toLowerCase().includes(query.toLowerCase());
  });
}
