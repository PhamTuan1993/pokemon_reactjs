import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import {
  getListPokemon,
  getPokemon,
  getPokemonFromUrl,
} from "../api/pokemonApi";
import parsePokemonData from "../utils/parsePokemonData";

export const getPokemonAction = createAsyncThunk(
  "pokemon/getPokemon",
  async (data) => {
    try {
      return await getPokemon(data);
    } catch (error) {
      return error;
    }
  }
);

export const getListPokemonAction = createAsyncThunk(
  "pokemon/getListPokemon",
  async (data, { dispatch }) => {
    dispatch(setLoading());
    try {
      const listPokemon = await getListPokemon(data);
      const arr = listPokemon.results.map((e) => getPokemon({ name: e.name }));
      dispatch(setLoading());
      return Promise.all(arr);
    } catch (error) {
      return error;
    }
  }
);

export const getPokemonUrlAction = createAsyncThunk(
  "pokemon/getPokemonUrl",
  async (data) => {
    try {
      return await getPokemonFromUrl(data);
    } catch (error) {
      return error;
    }
  }
);

export const pokemonSlice = createSlice({
  name: "pokemon",
  initialState: {
    pokemon: [],
    isLoading: false,
  },
  reducers: {
    resetState(state, { payload }) {
      if (payload?.length === 0) return;
      payload.forEach((e) => {
        state[e] = [];
      });
    },
    setLoading(state) {
      state.isLoading = !state.isLoading;
    },
  },
  extraReducers: {
    [getListPokemonAction.fulfilled]: (state, { payload }) => {
      if (payload) {
        const data = parsePokemonData(payload);
        state.pokemon = data;
      }
    },
    [getPokemonFromUrl.fulfilled]: (state, { payload }) => {
      console.log(payload);
    },
  },
});

export const { resetState, setLoading } = pokemonSlice.actions;
export const selectPokemon = (state) => state.pokemon.pokemon;
export const selectLoading = (state) => state.pokemon.isLoading;

export default pokemonSlice.reducer;
