import React, { Suspense } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import routes from "./config/router";
import PrivateRoute from "./components/PrivateRoute";
import 'bootstrap/dist/css/bootstrap.min.css';

export default function App() {
  return (
    <Suspense fallback="...">
      <Router>
        <Switch>
          {routes.map((e, i) =>
            e?.private ? (
              <PrivateRoute {...e} key={i} />
            ) : (
              <Route {...e} key={i} />
            )
          )}
        </Switch>
      </Router>
    </Suspense>
  );
}
