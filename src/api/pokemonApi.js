import axiosClient from "../config/axiosClient";

const baseUrl = process.env.REACT_APP_BASE_API;

const getPokemon = (params) => {
  const { name, ...rest } = params;
  if (!name) return;
  return axiosClient.get(`${baseUrl}pokemon/${params.name}?${rest}`);
};

const getListPokemon = (params) => {
  return axiosClient.get(`${baseUrl}pokemon`,{params});
};

const getPokemonFromUrl = (url) => {
  return axiosClient.get(url);
};

export { getPokemon, getListPokemon, getPokemonFromUrl};
