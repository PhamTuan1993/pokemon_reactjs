import { Redirect, Route } from "react-router-dom";
import useAuth from "../customHook/useAuth";

export default function PrivateRoute({ component, ...rest }) {
  const auth = useAuth();
  return (
    <Route
      render={() =>
        auth.user ? (
          component
        ) : (
          <Redirect
            to={{
              pathname: "/login",
            }}
          />
        )
      }
      {...rest}
    />
  );
}
