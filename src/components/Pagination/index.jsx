import React from "react";
import { Pagination } from "react-bootstrap";

export default function PaginationCus({ total, active, onPageChange }) {
  const item = [];

  const handlePageChange = (e) => {
    onPageChange(e);
  };

  const totalPage = () => {
    return Math.floor(total / 12);
  };

  for (let index = 1; index <= totalPage(); index++) {
    item.push(
      <Pagination.Item
        key={index}
        active={index === active}
        onClick={() => {
          handlePageChange(index);
        }}
      >
        {index}
      </Pagination.Item>
    );
  }
  return <Pagination>{item}</Pagination>;
}
