import React from "react";
import { Card, Col } from "react-bootstrap";

export default function PokemonItem({name, img}) {
  return (
    <Col xs={3} className="mb-5">
      <Card>
        <Card.Img src={img} />
        <Card.Body>
          <Card.Title>{name}</Card.Title>
        </Card.Body>
      </Card>
    </Col>
  );
}
